// @ts-check
/* eslint-env mocha */
import {analyze} from "eslint-scope";

Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})
describe('API', () => {

    before('start', () => {
        cy.visit('https://api.chucknorris.io');
    })

    it('show that the successful request returns a list of fixed categories containing n-amount of items', () => {
        cy.request('/jokes/categories').as('response')
            .then((response: any) => {
                expect(response.body).length.greaterThan(0) /// Checking that the array of categories is greater than zero
                expect(response).to.have.property('headers') // Checking that there is a headers field
            })
            .then((response: any) => {
                cy.writeFile('catalogs.json', JSON.stringify(response.body)) /// The Body save to file
            });

        cy.get('@response').then((r:any)=>{
            cy.readFile('catalogs.json').should('deep.equal', r.body); /// Comparing the saved body with the received one
        })
    })

    it('pick one of the categories from the step before and use it in the free text search request and make sure that each of the returned items contains the query string in the returned JSON.', () => {
        cy.readFile('catalogs.json').then((i:any)  => { // getting file
            cy.request('/jokes/search?query='+ i[0]) // send request with first catalog from file
                .as('response')
                .then((response: any) => {
                    expect(response.body.total).greaterThan(0); /// Checking that the totals of objects is greater than zero
                    expect(response.body.result).to.have.length(response.body.total); /// Comparing the declared number of objects with the actual number of objects
                })
        })
    })
})