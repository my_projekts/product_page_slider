// type definitions for Cypress object "cy"
/// <reference types="cypress" />
// type definition for out TodoModel
// @ts-check


context('Recommendation Carousel', () => {
    before(() => {
        cy.visit(Cypress.env("WEB_BASE_URL"));
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('The following error originated from your application code');
            return false;
        });
    })

    it('The recommendation Carousel should load all product cards and be visible', () => {
        cy.get('.z-header__topbar').should("be.visible");
        cy.get('.header-cart-link').should("be.visible");
        cy.get('.slick-slide').should("be.visible");
        cy.get('.product-title > .zcl-heading').should("be.visible");
        cy.get('.product-overview > .product-price > div > .product-price__price').should("be.visible");
        cy.get('.buy-box__inner').should("be.visible");
        cy.get('.zcl-carousel__title')
            .should("be.visible")
            .should("contain.text", "You May Also Like");
        cy.get(".flickity-slider .zcl-card").should("be.visible");

        cy.get(".flickity-slider .zcl-card").should(($p) => {
            expect($p.length).to.have.lessThan(16);
            expect($p.length).to.have.greaterThan(0);
        });

        cy.get(".flickity-slider .zcl-card .zcl-card__content").should("be.visible");
        cy.get(".flickity-slider .zcl-card .zcl-card__media").should("be.visible");
        cy.get(".flickity-slider .zcl-card .product-price").should("be.visible");
        cy.get(".flickity-slider .zcl-card .add-to-cart__button").should("be.visible");
        cy.get("[aria-label='Previous Page']").should("be.visible");
        cy.get("[aria-label='Next Page']").should("be.visible");
        cy.get(".zcl-carousel__pagination-text").should("be.visible");
    });

    it("Scroll to the (click) on Right arrow and assert that analytics events fired", () => {
        cy.get("[aria-label='Next Page']")
            .scrollIntoView()
            .should("be.visible")
            .click({force: true});
        cy.window().then((win:any) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');

            let event = win.dataLayer.find((el:any) => {
                return el.event = "productImpressions";
            });
            assert.isDefined(event, 'productImpressions in Data Layer is defined');

            let obj = win.dataLayer.find(function (el:any) {
                return el.event === 'productImpressions' &&
                    el.listLayout === "carousel" &&
                    el.ecommerce.impressions.length > 1;
            });
            assert.isTrue(obj.ecommerce.impressions.length >= 1 && obj.ecommerce.impressions.length <= 6, 'impressions in Data Layer is defined');
        })
    });

    it("Scroll Back (click) on Left arrow and assert that analytics event fired-When it scrolled back the product impression on step 3 above should not be fired", () => {
        cy.get("[aria-label='Previous Page']")
            .scrollIntoView()
            .should("be.visible")
            .click({force: true});

        cy.window().then((win:any) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');

            let event = win.dataLayer.find((el:any) => {
                return el.event = "productImpressions";
            });
            assert.isDefined(event, 'productImpressions in Data Layer is defined');

            let obj = win.dataLayer.find(function (el:any) {
                return el.event === 'productImpressions' &&
                    el.listLayout === "carousel" &&
                    el.ecommerce.impressions.length > 1;
            });
            assert.isTrue(obj.ecommerce.impressions.length >= 1 && obj.ecommerce.impressions.length <= 6, 'impressions in Data Layer is defined');

        })
    });

    it("Assert  < event: “setOriginalLocation” analytics event present on Datalayer", () => {
        cy.window().then((win:any) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
            let event = win.dataLayer.find((el:any) => {
                return el.event = "setOriginalLocation";
            });
            assert.isDefined(event, 'setOriginalLocationField in Data Layer is defined');
        })
    });

    it("Click on < Add to Cart > button  and assert that < event: ProductClick > and < event: addToCart > fired", () => {
        cy.get(".flickity-slider .zcl-card .add-to-cart__button")
            .first()
            .scrollIntoView()
            .should("be.visible")
            .click({force: true});

        cy.on('uncaught:exception', (/*err*/) => {
            return false;
        });

        cy.window().then((win:any) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
            let event = win.dataLayer.find((el:any) => {
                return el.event = "ProductClick";
            });
            assert.isDefined(event, 'ProductClick in Data Layer is defined');
        });

        cy.window().then((win:any) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
            let event = win.dataLayer.find((el:any) => {
                return el.event = "addToCart";
            });
            assert.isDefined(event, 'addToCart in Data Layer is defined');
        });
    });

});