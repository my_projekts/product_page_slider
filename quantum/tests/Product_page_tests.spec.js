import Product_controller from "../pages/controllers/Product_controller";
// This scenario should ensure that product cards LOADS and VISIBLE on
// the product recommendation carousel and all ANALYTICS EVENTS fired once
// the user click on the Left and Right arrows

context('Recommendation Carousel', () => {
    before(() => {
        cy.visit(Cypress.env("WEB_BASE_URL"));
        cy.on('uncaught:exception', (err) => {
            expect(err.message).to.include('The following error originated from your application code');
            return false;
        });
    })

    it('The recommendation Carousel should load all product cards and be visible ', () => {
        let pageElements = new Product_controller();
        pageElements.obj.header_toolbar().should("be.visible");
        pageElements.obj.header_cart_link().should("be.visible");
        pageElements.obj.product_image_slick().should("be.visible");
        pageElements.obj.product_name().should("be.visible");
        pageElements.obj.product_price().should("be.visible");
        pageElements.obj.product_buy_box().should("be.visible");
        pageElements.obj.carousel_title()
            .should("be.visible")
            .should("contain.text", "You May Also Like");
        pageElements.obj.slide_products().should("be.visible");

        pageElements.obj.slide_products().should(($p) => {
            expect($p.length).to.have.lessThan(16);
            expect($p.length).to.have.greaterThan(0);
        });

        pageElements.obj.slide_products_content().should("be.visible");
        pageElements.obj.slide_products_media().should("be.visible");
        pageElements.obj.slide_products_price().should("be.visible");
        pageElements.obj.slide_products_add_to_cart().should("be.visible");
        pageElements.obj.carousel_prev_page_button().should("be.visible");
        pageElements.obj.carousel_nex_page_button().should("be.visible");
        pageElements.obj.carousel_pagination().should("be.visible");
    });

    it("Scroll to the (click) on Right arrow and assert that analytics events fired", () => {
        let pageElements = new Product_controller();
        pageElements.obj.carousel_nex_page_button()
            .scrollIntoView()
            .should("be.visible")
            .click({force: true});
        cy.window().then((win) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
        })
    });

    it("Scroll Back (click) on Left arrow and assert that analytics event fired - " +
        "When it scrolled back the product impression on step 3 above should not be fired ", () => {
        let pageElements = new Product_controller();
        pageElements.obj.carousel_prev_page_button()
            .scrollIntoView()
            .should("be.visible")
            .click({force: true});
        cy.window().then((win) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
        })
    });

    it("Assert  < event: “setOriginalLocation” analytics event present on Datalayer", () => {
        cy.window().then((win) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
            let event = win.dataLayer.find((el) => {
                return el.event = "setOriginalLocation";
            });
            assert.isDefined(event, 'setOriginalLocationField in Data Layer is defined');
        })
    });

    it("Click on < Add to Cart > button  and assert that < event: ProductClick > and < event: addToCart > fired ", () => {
        let pageElements = new Product_controller();
        pageElements.obj.slide_products_add_to_cart()
            .first()
            .scrollIntoView()
            .should("be.visible")
            .click({force: true});

        cy.on('uncaught:exception', (/*err*/) => {
            return false;});

        cy.window().then((win) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
            let event = win.dataLayer.find((el) => {
                return el.event = "ProductClick";
            });
            assert.isDefined(event, 'ProductClick in Data Layer is defined');
        });

        cy.window().then((win) => {
            assert.isDefined(win.dataLayer, 'Data Layer is defined');
            let event = win.dataLayer.find((el) => {
                return el.event = "addToCart";
            });
            assert.isDefined(event, 'addToCart in Data Layer is defined');
        });
    });

})
