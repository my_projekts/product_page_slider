import scripts from "../constants/scripts.json";
context('Quantum test', () => {
    beforeEach(() => {
        cy.visit(Cypress.env("WEB_BASE_URL"));
    })

    let script = "(function() {\n" +
        "var qtm = document.createElement('script'); qtm.type = 'text/javascript'; qtm.async = 1; qtm.src = 'https://cdn.quantummetric.com/instrumentation/quantum-qa1.js';\n" +
        "var d = document.getElementsByTagName('script')[0];\n" +
        "!window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d); })();\n" +
        "window[\"QuantumMetricOnload\"] = function() {\n" +
        "var sessionID = QuantumMetricAPI.getSessionID();\n" +
        "console.log('Logger:' + sessionID) };"

    let script2 = "(function() {\n" +
        "var qtm = document.createElement('script'); qtm.type = 'text/javascript'; qtm.async = 1; qtm.src = 'https://cdn.quantummetric.com/instrumentation/quantum-qa1.js';\n" +
        "var d = document.getElementsByTagName('script')[0];\n" +
        "!window.QuantumMetricAPI && d.parentNode.insertBefore(qtm, d); })();\n" +
        "window[\"QuantumMetricOnload\"] = function() { QuantumMetricAPI.addEventListener('start', function() {\n" +
        "var sessionID = QuantumMetricAPI.getSessionID();\n" +
        "console.log('Logger:' + sessionID) });" +
        "return sessionID\n" +
        "};"



    it('Script_one', () => {
        cy.get("html").then(() => {
            let s = document.createElement("script");
            s.type = "text/javascript";
            s.text = scripts.script_one;
            cy.log(s);
            Cypress.$("head").append(s);
        });

    });

    it('Script_two', () => {
        cy.get("html").then(() => {
            let s = document.createElement("script");
            s.className = "script_injected";
            s.type = "text/javascript";
            s.text = scripts.script_two;
            cy.log(s);
            Cypress.$("head").append(s);
        });
        Cypress.$("sessionID").get((data)=>{
            cy.log("Dqata " +data);
        });

    });

})
