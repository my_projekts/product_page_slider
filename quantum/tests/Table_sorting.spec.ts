// type definitions for Cypress object "cy"
/// <reference types="cypress" />
// type definition for out TodoModel
// @ts-check
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
    // failing the test
    return false
})


describe('TAble sorting', () => {
    before('Open url ', () => {
        cy.visit('https://the-internet.herokuapp.com/tables')
    })

    it('test sorting of last names by alphabetic order (A..Z, Z..A)', () => {
        let beforeSorting: string[] = []; // create new array
        cy.get('#table1 tbody tr') /// getting every row of the table
            .should('be.visible') /// expect lines to display
            .each(($el, $index) => { ///refer to a string object
                beforeSorting.push(
                    $el.find('td').first().text() ////get all the columns of a row, take the first column and get the value from it. I write to the array declared earlier.
                );
            })

        cy.get('#table1 thead tr th') /// get all columns
            .should('be.visible')
            .contains("Last Name") /// Find  the desired column by value
            .click({force: true}); // click it

        /*Add to array after sorting*/
        let afterSorting: string[] = [];
        cy.get('#table1 tbody tr').each(($el, $index) => {
            afterSorting.push($el.find('td').first().text());
        })

        cy.wrap(afterSorting).should("deep.not.eq", beforeSorting.sort()); /// I compare the array with the column values ​​before sorting and after sorting
        cy.wrap(afterSorting).should("deep.eq", afterSorting.sort()); /// I take an array of data before sorting, sort it and compare it with the sorted array. This makes it clear that the array was sorted correctly.
    })

    /*Here algorithm is the same as in the first test, only I check the field with index 1. => .eq(1)*/
    it('test sorting of first names by alphabetic order (A..Z, Z..A)', () => {
        let beforeSorting: string[] = [];
        cy.get('#table1 tbody tr')
            .should('be.visible')
            .each(($el, $index) => {
                beforeSorting.push($el.find('td').eq(1).text());
            })

        cy.get('#table1 thead tr th')
            .should('be.visible')
            .contains("First Name")
            .click({force: true});

        let afterSorting: string[] = [];
        cy.get('#table1 tbody tr').each(($el, $index) => {
            afterSorting.push($el.find('td').eq(1).text());
        })

        cy.wrap(afterSorting).should("deep.not.eq", beforeSorting.sort());
        cy.wrap(afterSorting).should("deep.eq", afterSorting.sort());

    })
})

