class Product_object {
    header_toolbar() {
        return cy.get('.z-header__topbar');
    }

    header_cart_link() {
        return cy.get('.header-cart-link');
    }

    product_image_slick() {
        return cy.get('.slick-slide');
    }

    product_name() {
        return cy.get('.product-title > .zcl-heading');
    }

    product_price() {
        return cy.get('.product-overview > .product-price > div > .product-price__price');
    }

    product_buy_box() {
        return cy.get('.buy-box__inner');
    }

    carousel_title() {
        return cy.get('.zcl-carousel__title');
    }

    slide_products() {
        return cy.get(".flickity-slider .zcl-card");
    }

    slide_visible_products() {
        return cy.get(".flickity-slider .zcl-card.is-selected");
    }

    slide_products_content() {
        return cy.get(".flickity-slider .zcl-card .zcl-card__content");
    }

    slide_products_media() {
        return cy.get(".flickity-slider .zcl-card .zcl-card__media");
    }

    slide_products_price() {
        return cy.get(".flickity-slider .zcl-card .product-price");
    }

    slide_products_add_to_cart() {
        return cy.get(".flickity-slider .zcl-card .add-to-cart__button");
    }

    carousel_prev_page_button() {
        return cy.get("[aria-label='Previous Page']")
    }

    carousel_nex_page_button() {
        return cy.get("[aria-label='Next Page']")
    }

    carousel_pagination() {
        return cy.get(".zcl-carousel__pagination-text")
    }
}

export default Product_object;